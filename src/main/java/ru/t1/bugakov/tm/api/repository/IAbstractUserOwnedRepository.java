package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @NotNull
    M add(@NotNull final String userId, @NotNull final M model);

    @NotNull
    List<M> findAll(@NotNull final String userId);

    @NotNull
    List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator);

    int getSize(@NotNull final String userId);

    @Nullable
    M findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    M findByIndex(@NotNull final String userId, @NotNull final Integer index);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

    void clear(@NotNull final String userId);

    @Nullable
    M remove(@Nullable final String userId, @Nullable final M model);

    @Nullable
    M removeById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    M removeByIndex(@NotNull final String userId, @NotNull final Integer index);

}
